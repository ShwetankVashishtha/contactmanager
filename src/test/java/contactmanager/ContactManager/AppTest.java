package contactmanager.ContactManager;

import org.testng.annotations.Test;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

public class AppTest {
	private static AndroidDriver<MobileElement> driver;

	@Test
	public void f() throws InterruptedException {
		driver.findElementByAccessibilityId("Add Contact").click();
		driver.findElementById("com.example.android.contactmanager:id/contactNameEditText").clear();
		driver.findElementById("com.example.android.contactmanager:id/contactNameEditText").sendKeys("Shwetank");
		driver.findElementById("com.example.android.contactmanager:id/contactPhoneEditText").clear();
		driver.findElementById("com.example.android.contactmanager:id/contactPhoneTypeSpinner").click();
		Thread.sleep(3000);
		driver.findElementById("android:id/text1").click();
		Thread.sleep(3000);
		driver.findElementById("com.example.android.contactmanager:id/contactPhoneEditText").sendKeys("9045631832");
		driver.findElementById("com.example.android.contactmanager:id/contactEmailEditText").clear();
		driver.findElementById("com.example.android.contactmanager:id/contactEmailEditText")
				.sendKeys("vashishthashwetank@gmail.com");
		driver.findElementByAccessibilityId("Save").click();
		Thread.sleep(10000);
		driver.quit();
	}

	@BeforeClass
	public void beforeMethod() throws MalformedURLException, InterruptedException {
		File classpathRoot = new File(System.getProperty("user.dir"));
		File appDir = new File(classpathRoot, "/test");
		File app = new File(appDir, "ContactManager.apk");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		capabilities.setCapability("deviceName", "Nexus7");
		capabilities.setCapability("platformVersion", "7.1.1");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.example.android.contactmanager");
		capabilities.setCapability("appActivity", "com.example.android.contactmanager.ContactManager");
		driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		Thread.sleep(10000);
	}

	@AfterMethod
	public void afterMethod() {

	}

}